$(function() {

/* SLIDER  */
    const mySwiper = new Swiper ('.swiper-container', {
        direction : 'horizontal',
        spaceBetween : 0,
        slidesPerView: 1,
        loop : true,
        stopOnLastSlide : false,
        speed : 800,
        autoplay : {
        delay: 1500,
        disableOnInteraction: false
      },
      pagination: {
        el: '.swiper-pagination',
        clickable: true,
      },
      navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
      },
    })

    
    $('.slider-text').slick({
      arrows: false,
      dots: true,
      infinite: true,
      asNavFor: '.slider-foto',
      autoplay: true,
      slidesToShow: 1,
      appendDots:$('.offer__dots')
    });

    $('.slider-foto').slick({
        arrows: false,
        asNavFor: '.slider-text'
    });


/*sticky-header*/
var header = $('.js-header'),
    cloneHeader = header.clone();


cloneHeader.addClass('header--fixed');
header.before(cloneHeader);


$(window).scroll(function() {

    console.log($(window).scrollTop());

    if ( $(window).scrollTop() > 250 ) {
        cloneHeader.addClass('header--is-show');
    } else {
        cloneHeader.removeClass('header--is-show');
    }

});

  
/* HAMBURGER*/
$('.hamburger').on('mouseup',function(){
  $('.menu-pop-up').toggle();
})

// $('#closeMenu').on('click',function(){
//   $('.popup-menu').hide();
// });


    
/* Popup-menu*/
  $('.menu__item_shop').on('mouseover',function(){
    $('.menu__shop').show();
  })
  $('.menu__item_shop').on('mouseout',function(){
    $('.menu__shop').hide();
  })
  
/* Modal window*/
  $('.callback-icon').on('click', function() {
    $('.wrapper-modal, .modal-window').fadeIn();
  });

  $('.wrapper-modal, .menu-close, .thanks-window__btn').on('click', function() {
    $('.wrapper-modal, .modal-window, .thanks-window').fadeOut();
  });

  $('.wrapper-modal').children().on('click', function(e){
    e.stopPropagation();
  })

/* Validate and submitting the form*/
  $('[data-submit]').on('click', function(e) {
        e.preventDefault();
        $(this).parent('form').submit();
    })


  $.validator.addMethod('regex', function(value, element, regexp){
      var re = new RegExp(regexp);
      return this.optional(element) || re.test(value);
  },
  "Пожалуйста, проверьте ввод"
  );

  $("#modalForm").validate({
      rules: {
                tel: {
                    digits : true,
                    required: true,
                    minlength: 10,
                    maxlength: 11,
                    regex: "[0-9]+"
                },
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                tel: {
                    required: "Пожалуйста, заполните это поле",
                    regex: "Пожалуйста, укажите номер"
                },
                name: {
                    required: "Пожалуйста, заполните это поле",
                },
                email: {
                    required: "Пожалуйста, заполните это поле",
                    regex: "Пожалуйста, укажите верный email"
                }
            },

            submitHandler: function(form) {
                var $form = $(form);
                var $formId = $(form).attr('id');
                switch ($formId) {
                    // Если у формы id="modalForm" - делаем:
                    case 'modalForm':
                        $.ajax({
                                type: 'POST',
                                url: $form.attr('action'),
                                data: $form.serialize()
                            })
                            .done(function() {
                                console.log('Success');
                            })
                            .fail(function() {
                                console.log('Fail');
                            })
                            .always(function() {
                                console.log('Always');
                                setTimeout(function() {
                                    $form.trigger('reset');
                                    $('.modal-window').fadeOut();
                                }, 1000);
                                setTimeout(function() {
                                    $('.wrapper-modal, .thanks-window').fadeIn();
                                }, 1400);
                            });
                        break;
                }
                return false;
            }
  })
  

    console.log('Готово');

});

  /* Scroll*/

var $page = $('html, body');
$('a[href*="#сollection"]').click(function() {
    $page.animate({
        scrollTop: $($.attr(this, 'href')).offset().top
    }, 1000);
    return false;
});



